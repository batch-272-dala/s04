-- 1. Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT albumTitle, songName, length FROM albums JOIN songs ON albums.id = songs.albumId;

-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT albumTitle, name FROM albums JOIN artists ON artists.id = albums.artistId WHERE albumTitle LIKE "%a%";

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY albumTitle DESC LIMIT 4;

-- 6. Join the 'albums' and 'songs' tables.(Sort albums from Z-A.)
SELECT albumTitle, songName FROM albums JOIN songs ON albums.id = songs.albumId ORDER BY albumTitle DESC;
